### Popis ###
Aplikace využívá plovoucí widget pro rychlý přístup k oblíbeným aplikacím, kontaktům a stránkám.

### Obrázky ###
![Screenshot_2015-01-02-22-19-53.png](https://bitbucket.org/repo/xGAMEe/images/2670119717-Screenshot_2015-01-02-22-19-53.png)
![Screenshot_2015-01-02-22-40-06.png](https://bitbucket.org/repo/xGAMEe/images/261844303-Screenshot_2015-01-02-22-40-06.png)
![Screenshot_2015-01-02-22-08-39.png](https://bitbucket.org/repo/xGAMEe/images/822890466-Screenshot_2015-01-02-22-08-39.png)
![Screenshot_2015-01-02-22-36-28.png](https://bitbucket.org/repo/xGAMEe/images/4277660704-Screenshot_2015-01-02-22-36-28.png)




### Informace ###
* Version 0.3

* Tomáš Lála
* email: Tom.lala@seznam.cz