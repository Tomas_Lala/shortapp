package cz.lala.shortapp.database;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import cz.lala.shortapp.R;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.ShortApp;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract.Contacts;
import android.util.Log;

public class AppsChecker  extends AsyncTask<Void, Void, Void>{
	
	private List<ResolveInfo> installedAppsList;
	private List<ShortApp> packageInfos = new ArrayList<ShortApp>();
	private Context context;
	private AppDatabase database;
	private OnAppsCheckerDone checkerDone;
	public interface OnAppsCheckerDone
	{
		public void onAppsCheckDone();
	}
	
	public AppsChecker(Context context, OnAppsCheckerDone checkerDone) {
		this.context = context;
		this.checkerDone = checkerDone;
		database = new AppDatabase(context);		
	}	
	@Override
	protected Void doInBackground(Void... params) {
		final PackageManager pm = context.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        installedAppsList = pm.queryIntentActivities(mainIntent, 0);
        Collections.sort(installedAppsList, new ResolveInfo.DisplayNameComparator(pm));
        
        int i = 0;
        List<HashMap<String, String>> favouriteShortAppsList = new AppsSaver(context).getAppsFromDatabase();
		for (HashMap<String, String> hashMap: favouriteShortAppsList) {
			for (ResolveInfo resolveInfo: installedAppsList) {
				if ((hashMap.get(ShortApp.SHORTAPP_KIND).equals(AppDatabase.KIND_APPLIACATION))) {		
					/// Pokud je ShortApp nainstalovaná
					if (resolveInfo.activityInfo.packageName.equals(hashMap.get(ShortApp.SHORTAPP_PACKAGE_NAME))) {
						Log.i("ShortApp_AppsChecker", resolveInfo + "set as favourite.");
						this.packageInfos.add(new ShortApp(resolveInfo.loadLabel(context.getPackageManager()).toString(),
								resolveInfo.activityInfo.packageName, hashMap.get(ShortApp.SHORTAPP_KIND), resolveInfo.loadIcon(context.getPackageManager()),0));
						break;
					}
					/// Smazání oblíbených aplikací z databáze, které jsou již odinstalované
					if (i==installedAppsList.size()-1) {
						Log.i("ShortApp_AppsChecker", hashMap + "deleted from favourite");
						new AppsSaver(context).removeFavouriteApp(hashMap.get(ShortApp.SHORTAPP_PACKAGE_NAME), false,0);	
					}
				}
				else
				{
					Log.i("ShortApp_AppsChecker", resolveInfo + "set as favourite.");
					Drawable icon;
					icon = getIcon(hashMap.get(ShortApp.SHORTAPP_PATHICON),hashMap.get(ShortApp.SHORTAPP_KIND));
					
					this.packageInfos.add(new ShortApp(hashMap.get(ShortApp.SHORTAPP_NAME),
							hashMap.get(ShortApp.SHORTAPP_PACKAGE_NAME), hashMap.get(ShortApp.SHORTAPP_KIND), icon,0));
					break;
				}
			}
			i++;
		}
        
		return null;
	}
	@Override
	protected void onPostExecute(Void result) {
	AppsSaver.setInstalledAppsList(installedAppsList);
	AppsSaver.setFavouriteAppsList(packageInfos);
	checkerDone.onAppsCheckDone();	
		super.onPostExecute(result);
	}
	
	private Drawable getIcon(String pathIcon, String shortappKind)
	{
		if (pathIcon.equals(AppDatabase.NULL)) {
			Log.i("ShortApp_AppsChecker", "BIN");
			return context.getResources().getDrawable( R.drawable.ic_www_icon);
		}
		if (shortappKind.equals(AppDatabase.KIND_CONTACT)) {
			Cursor cursor = context.getContentResolver().query(Uri.parse(pathIcon),
      	          new String[] {Contacts.Photo.PHOTO}, null, null, null);
      	     if (cursor == null) {
      	    	return context.getResources().getDrawable( R.drawable.ic_contacts_icon);
      	     }
      	     
			try {
      	         if (cursor.moveToFirst()) {
      	             byte[] data = cursor.getBlob(0);
      	             if (data != null) {
      	            	return Drawable.createFromStream( new ByteArrayInputStream(data), "CurvedBG");
      	             }
      	         }
      	     } finally {
      	         cursor.close();
      	     }    		
          		return context.getResources().getDrawable( R.drawable.ic_garbage_bin_icon);

		}
		return null;
	}

}
