package cz.lala.shortapp.database;

import java.util.ArrayList;
import java.util.HashMap;

import cz.lala.shortapp.R;
import android.R.integer;
import android.content.Context;

public class Icon_Database {
	
	public static final String ICONS = "Icons";
	public static final int DEFAULT= 0;
	public static final int CAR= 1;
	public static final int CAT= 2;
	public static final int DOG= 3;
	
	private int[] default_icons;
	private int[] car_icons;
	
	private ArrayList<int[]> arrayList;
			
	public Icon_Database(Context context){
		default_icons = new int[]{R.drawable.ic_minion_bananas_icon, R.drawable.ic_minion_amazed_icon, R.drawable.ic_minion_dancing_icon};
		car_icons = new int[]{R.drawable.ic_santa_christmas_tree_icon, R.drawable.ic_santa_icon, R.drawable.ic_santa_cat_icon};
		arrayList = new ArrayList<int[]>();
	
	}
	public int[] getIcons(int kind)
	{
		switch (kind) {
		case DEFAULT:
			return default_icons;
			
		case CAR:
			return car_icons;

		default:
			break;
		}
		return null;
	}
	public  ArrayList<int[]> getAllIcons()
	{
		arrayList.add(DEFAULT, default_icons);
		arrayList.add(CAR, car_icons);
		
		return arrayList;
	}

}
