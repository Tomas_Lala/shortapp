package cz.lala.shortapp.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.lala.shortapp.setting.ShortApp;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppDatabase extends SQLiteOpenHelper{
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "AppDatabase.db";
	private static final String TABLE_NAME = "AppDatabase";
	private static final String COLUMN_ID = "id";
	private static final String COLUMN_PACKAGE_NAME = "package";
	private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SHORTAPP_KIND = "kind";
    private static final String COLUMN_PATHICON = "pathIcon";
    
    public static final String KIND_APPLIACATION = "APP";
    public static final String KIND_URL = "URL";
    public static final String KIND_CONTACT = "CONTACT: ";
    public static final String KIND_TWITTER = "TWITTER";
    public static final String KIND_FB_PAGE = "FB_PAGE";
    public static final String KIND_FB_GROUP = "FB_GROUP";
    public static final String KIND_FB_USER = "FB_USER";
    public static final String KIND_FB_MES_USER = "FB_MES_USER";
    public static final String KIND_FB_MES_GROUP = "FB_MES_GROUP";
    		 
    private static final String SELECT_ALL_PACKAGES= "SELECT * FROM " + TABLE_NAME;
    private static final String DELETE_PACKAGE_BY_NAME= COLUMN_PACKAGE_NAME + " = ";
    private static final String TEXT_TYPE = " TEXT";
    public static final String NULL = " NULL";
   
    private static final String SQL_CREATE_ENTRIES =
        "CREATE TABLE " + AppDatabase.TABLE_NAME + " (" +
        AppDatabase.COLUMN_ID + " INTEGER PRIMARY KEY," +
        AppDatabase.COLUMN_PACKAGE_NAME + TEXT_TYPE+","+
        AppDatabase.COLUMN_NAME + TEXT_TYPE + "," +
        AppDatabase.COLUMN_SHORTAPP_KIND + " INTEGER"+"," +
        AppDatabase.COLUMN_PATHICON + TEXT_TYPE + 
        " )";
    private static final String SQL_DELETE_ENTRIES =
        "DROP TABLE IF EXISTS " + AppDatabase.TABLE_NAME;

    
    public AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		  db.execSQL(SQL_CREATE_ENTRIES);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
		
	}
	
	/// Uložení package jednotlivé aplikace
	public void saveNewShortAppp(String appPackage,String name,String kind,String pathIcon)
	{
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(COLUMN_PACKAGE_NAME, appPackage);
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_SHORTAPP_KIND, kind);
		values.put(COLUMN_PATHICON, pathIcon);
		Log.i("ShortApp_Database", name + " saved");
		
		database.insert(TABLE_NAME, null, values);
		database.close();
	}
	
	/// Vytažení všech uložených aplikací
	public List<HashMap<String, String>> readAllShortApps()
	{
		SQLiteDatabase database = this.getReadableDatabase();
		HashMap<String, String> hashMap ;
		List<HashMap<String, String>> appPackagelist = new ArrayList<HashMap<String, String>>();
		Cursor cursor = database.rawQuery(SELECT_ALL_PACKAGES, null);
		if (cursor.moveToFirst()) {
			 do {
				 hashMap= new HashMap<String,String>();
				 hashMap.put(ShortApp.SHORTAPP_PACKAGE_NAME, cursor.getString(cursor.getColumnIndex(COLUMN_PACKAGE_NAME)));
				 hashMap.put(ShortApp.SHORTAPP_NAME, cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
				 hashMap.put(ShortApp.SHORTAPP_KIND, cursor.getString(cursor.getColumnIndex(COLUMN_SHORTAPP_KIND)));
				 hashMap.put(ShortApp.SHORTAPP_PATHICON, cursor.getString(cursor.getColumnIndex(COLUMN_PATHICON)));
				 
				 appPackagelist.add(hashMap);
		        } while (cursor.moveToNext());
		}
		database.close();
		return appPackagelist;
	}
	
	/// Smazaní apliakce podle package
	public void deleteShortApp(String appPackage)
	{
		 SQLiteDatabase database = this.getWritableDatabase();
		 String[] whereArgs = new String[] { appPackage.replaceAll("\\.", "\\.") };
		 database.delete(TABLE_NAME, DELETE_PACKAGE_BY_NAME + "?", whereArgs);
		 database.close();
	}

}
