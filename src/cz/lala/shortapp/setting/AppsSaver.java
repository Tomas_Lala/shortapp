package cz.lala.shortapp.setting;


import java.util.HashMap;
import java.util.List;

import cz.lala.shortapp.database.AppDatabase;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

public class AppsSaver {
	
	
	
	private static List<ResolveInfo> installedAppsList;
	private static List<ShortApp> favouriteAppsList ;
	private AppDatabase database;
	
	public AppsSaver(Context context) {
		database = new AppDatabase(context);
	}
	
	public static List<ResolveInfo> getInstalledAppsList() {
		return installedAppsList;
	}
	public static void setInstalledAppsList(List<ResolveInfo> installedAppsList) {
		AppsSaver.installedAppsList = installedAppsList;
	}
	
	public static List<ShortApp> getFavouriteAppsList() {
		return favouriteAppsList;
	}
	public static void setFavouriteAppsList(List<ShortApp> favouriteAppsList) {
		AppsSaver.favouriteAppsList = favouriteAppsList;
	}
	public static int getCountFavouriteAppsList()
	{
		return AppsSaver.favouriteAppsList.size();
	}
	public void setFavouriteAppToDatabase(String pathName,String name,String kind,Drawable icon,int intIcon,String pathIcon)
	{
		database.saveNewShortAppp(pathName, name, kind,pathIcon);
		if (icon!=null) {
			favouriteAppsList.add(new ShortApp(name, pathName, kind, icon,0));
		}else  {
			favouriteAppsList.add(new ShortApp(name, pathName, kind, null,intIcon));
		}
		
	}
	public void removeFavouriteApp(String packageName, boolean isInFavouriteAppsList, int position)
	{
			database.deleteShortApp(packageName);
			if (isInFavouriteAppsList) {
				favouriteAppsList.remove(position);
			}
			
	}
	public static boolean isAlreadyFavouriteApp(String pathName)
	{
		for (int i = 0; i < AppsSaver.favouriteAppsList.size(); i++) {
			if (pathName.equals(AppsSaver.favouriteAppsList.get(i).getPathName())) {
				return true;
			}
		}
		return false;
	}
	public List<HashMap<String, String>> getAppsFromDatabase()
	{
		return database.readAllShortApps();
	}

}
