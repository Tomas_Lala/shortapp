package cz.lala.shortapp.setting.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.service.textservice.SpellCheckerService.Session;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockDialogFragment;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppDatabase;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.ShortApp;

public class Fb_Page_Dialog extends SherlockDialogFragment {
	
	ShortApp shortApp;
	Context context;
	EditText name;
	EditText pathName;
	ImageView imageView;

	public Fb_Page_Dialog(Context context,ShortApp shortApp) {
		this.context = context;
		this.shortApp = shortApp;
		setRetainInstance(true);
	}
	 
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getSherlockActivity());
		LayoutInflater inflater = getSherlockActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dl_webpage, null);
        name = (EditText) view.findViewById(R.id.fr_dialog_name);
        pathName = (EditText) view.findViewById(R.id.fr_dialog_path);
        imageView = (ImageView)view.findViewById(R.id.fr_dialog_image);
		
		builder.setView(view).setTitle(shortApp.getName())
	           .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	if (name.getText().toString().length()==0||pathName.getText().toString().length()==0) {
	            		Toast.makeText(context, "Please fill it!", Toast.LENGTH_SHORT).show();
					
	            	}
	            	else if (AppsSaver.getCountFavouriteAppsList()==10) {
	           			Toast.makeText(getSherlockActivity(), "Opps.. Too many ShortApps.", Toast.LENGTH_SHORT).show();
	           			return;
	           		}
	            	else if (AppsSaver.isAlreadyFavouriteApp(pathName.getText().toString())) {
	           			Toast.makeText(getSherlockActivity(), "This one has already setted.", Toast.LENGTH_SHORT).show();
	           			return;
	           		}
	           		else{
	           			
	           			new AppsSaver(getSherlockActivity()).setFavouriteAppToDatabase("http://"+pathName.getText().toString(),name.getText().toString(),shortApp.getKind(),null,R.drawable.ic_garbage_bin_icon,AppDatabase.NULL);
	           			Toast.makeText(getSherlockActivity(),name.getText().toString() +" set as ShortApp", Toast.LENGTH_SHORT).show();
	           		}
	               }
	           })
	           .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                   Fb_Page_Dialog.this.getDialog().cancel();
	               }
	           });      
	    return builder.create();
	}

/*	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	    	System.out.println("Connected");
	    } else if (state.isClosed()) {
	    	System.out.println("Not coon");
	    }
	}*/
}
