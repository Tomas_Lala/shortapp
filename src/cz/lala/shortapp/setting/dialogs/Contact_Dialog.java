package cz.lala.shortapp.setting.dialogs;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockDialogFragment;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppDatabase;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.ShortApp;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public  class Contact_Dialog extends SherlockDialogFragment {
	
	ShortApp shortApp;
	Context context;
	AutoCompleteTextView name;
	ImageView imageView;
	 Cursor phones ;
	 List<String> names;
	 ArrayList<String>  paths;
	 ArrayList<String>  imagespath;
	 ArrayAdapter adapter;
	 private boolean contactsetted = false;
	 private Uri photoUri;
	 Drawable drawable = null;
	 
	public Contact_Dialog(Context context,ShortApp shortApp) {
		this.context = context;
		this.shortApp = shortApp;
		setRetainInstance(true);
	}
	 
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getSherlockActivity());
		LayoutInflater inflater = getSherlockActivity().getLayoutInflater();
		
		paths = new ArrayList<String>();
        final View view = inflater.inflate(R.layout.dl_contacts, null);
        phones = getSherlockActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        names  = new ArrayList<String>();
       imagespath  = new ArrayList<String>();
        while (phones.moveToNext())
        {
        names.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
        paths.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)));

        }
        name = (AutoCompleteTextView) view.findViewById(R.id.dl_contacts_name);
        imageView = (ImageView)view.findViewById(R.id.dl_contacts_image);
        
        
        adapter = new ArrayAdapter  
        	      (getSherlockActivity(),android.R.layout.select_dialog_item, names);
        name.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (names.contains(s.toString())) {
					contactsetted = true;
				}
				else
				{
					drawable = context.getResources().getDrawable(R.drawable.ic_garbage_bin_icon);
            		imageView.setBackgroundResource(R.drawable.ic_garbage_bin_icon);
					contactsetted = false;
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
       
        name.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                    long id) {
            	contactsetted =true;
            	
            	try {
            		
            		 Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, Long.parseLong(paths.get(names.indexOf(name.getText().toString()))));
            	    photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
            	     Cursor cursor = getSherlockActivity().getContentResolver().query(photoUri,
            	          new String[] {Contacts.Photo.PHOTO}, null, null, null);
            	     if (cursor == null) {
            	         return;
            	     }
            	     try {
            	         if (cursor.moveToFirst()) {
            	             byte[] data = cursor.getBlob(0);
            	             if (data != null) {
            	                 drawable  = Drawable.createFromStream( new ByteArrayInputStream(data), "CurvedBG");
            	             }
            	         }
            	     } finally {
            	         cursor.close();
            	     }
				 
            		
                	if (drawable!=null) {
                		imageView.setBackground(drawable);
                		
					}
                	else {
                		drawable = context.getResources().getDrawable(R.drawable.ic_garbage_bin_icon);
                		imageView.setBackgroundResource(R.drawable.ic_garbage_bin_icon);
                		
					}
                	view.requestLayout();
            	}catch (Exception e) {
    					e.printStackTrace();
    				}


            }
        });
		name.setAdapter(adapter);
		builder.setView(view).setTitle(shortApp.getName())
	           .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	 if (contactsetted==false) {
		            		Toast.makeText(context, "Need to set contact", Toast.LENGTH_SHORT).show();
		            		return;
		            }   
	            	if (name.getText().toString().length()==0) {
	            		Toast.makeText(context, "Please fill it!", Toast.LENGTH_SHORT).show();
					
	            	}
	            	else if (AppsSaver.getCountFavouriteAppsList()==10) {
	           			Toast.makeText(getSherlockActivity(), "Opps.. Too many ShortApps.", Toast.LENGTH_SHORT).show();
	           			return;
	           		}
	            	else if (AppsSaver.isAlreadyFavouriteApp(paths.get(names.indexOf(name.getText().toString())))) {
	           			Toast.makeText(getSherlockActivity(), "This one has already setted.", Toast.LENGTH_SHORT).show();
	           			return;
	           		}
	           		else{
	           			
	           			new AppsSaver(getSherlockActivity()).setFavouriteAppToDatabase(AppDatabase.KIND_CONTACT + paths.get(names.indexOf(name.getText().toString())),name.getText().toString(),shortApp.getKind(),drawable,0,photoUri.toString());
	           			Toast.makeText(getSherlockActivity(),name.getText().toString() +" set as ShortApp", Toast.LENGTH_SHORT).show();
	           		}
	               }
	           })
	           .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                   Contact_Dialog.this.getDialog().cancel();
	               }
	           });      
	    return builder.create();
	}
	                
	 
}
