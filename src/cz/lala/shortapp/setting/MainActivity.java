package cz.lala.shortapp.setting;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import cz.lala.shortapp.R;
import cz.lala.shortapp.service.Windowservice;
import cz.lala.shortapp.setting.addapps.Fr_SelectShortApps;
import cz.lala.shortapp.setting.showapps.Fr_MyShortApps;

public class MainActivity extends SherlockFragmentActivity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			stopService(new Intent(this, Windowservice.class));
		} catch (Exception e) {}
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new Fr_MyShortApps()).commit();
		
		}
	}
	
	@Override
	public void onBackPressed() {
		 if (getFragmentManager().findFragmentByTag("TO_B") != null) {
			   getFragmentManager().popBackStack("FROM_B_TO_A",
		       FragmentManager.POP_BACK_STACK_INCLUSIVE);
		   }
		 else
		 {
		super.onBackPressed();
		 }
	}
	@Override
	protected void onStart() {
		try {
			stopService(new Intent(getApplicationContext(), Windowservice.class));
		} catch (Exception e) {}
		super.onStart();
	}
	@Override
	protected void onStop() {
		super.onStop(); 	 
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();		
	}

}
