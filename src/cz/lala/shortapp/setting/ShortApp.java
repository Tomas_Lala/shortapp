package cz.lala.shortapp.setting;

import android.graphics.drawable.Drawable;

public class ShortApp {
	
	public static final String SHORTAPP_PACKAGE_NAME = "shortapp_package_name";
	public static final String SHORTAPP_NAME = "shortapp_name";
	public static final String SHORTAPP_KIND = "shortapp_kind";
	public static final String SHORTAPP_PATHICON = "shortapp_pathIcon";
	
	private String name;
	private String pathName;
	private String kind;
	private Drawable icon;
	private int intIcon;
	
	public ShortApp() {}
	
	public ShortApp(String name, String pathName,String kind,Drawable icon,int intIcon) {
		setIcon(icon);
		setKind(kind);
		setName(name);
		setPathName(pathName);
		this.intIcon = intIcon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPathName() {
		return pathName;
	}
	public void setPathName(String pathName) {
		this.pathName = pathName;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public Drawable getIcon() {
		return icon;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	public int getIntIcon() {
		return intIcon;
	}

	public void setIntIcon(int intIcon) {
		this.intIcon = intIcon;
	} 

}
