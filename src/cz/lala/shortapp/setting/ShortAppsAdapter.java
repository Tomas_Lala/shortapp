package cz.lala.shortapp.setting;

import java.util.List;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import cz.lala.shortapp.R;
import cz.lala.shortapp.setting.showapps.Fr_MyShortApps.BtnClickListener;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ShortAppsAdapter extends BaseAdapter {
	
	public static final int DELETE=1;
	
	private BtnClickListener mClickListener = null;
	Context context;
	List<ShortApp> arrayShortApps;
	private LayoutInflater inflater;
	private int showbuttons;
	private List<ResolveInfo> infos;
	
	public ShortAppsAdapter(Context context,List<ShortApp> arrayShortApps,int showbuttons,BtnClickListener listener) {
		mClickListener = listener;
		this.context = context;
		this.arrayShortApps = arrayShortApps;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.showbuttons = showbuttons;
	}

	public ShortAppsAdapter( int showbuttons,Context context,
			List<ResolveInfo> infos) {
		this.context = context;
		this.infos = infos;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.showbuttons = showbuttons;
	}

	@Override
	public int getCount() {
		  if (arrayShortApps!=null) {
		return arrayShortApps.size();
		  }
		  if (infos!=null) {
			  return infos.size();
		  }
		  return 0;
	}

	@Override
	public Object getItem(int position) {
		  if (arrayShortApps!=null) {
			  return arrayShortApps.get(position);
		  }
			if (infos!=null) {
				return infos.get(position);
			}
		return 0;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 View layout = convertView;
	        if (layout == null)
	        layout = inflater.inflate(R.layout.shortapprow, parent,false);
	        TextView name = (TextView)layout.findViewById(R.id.shortapprow_name);
	        TextView packageName = (TextView)layout.findViewById(R.id.shortapprow_package_name);
	        ImageView image = (ImageView)layout.findViewById(R.id.shortapprow_image);
	        ImageView deleteImage = (ImageView)layout.findViewById(R.id.shortapprow_delete);
	        if (arrayShortApps!=null) {
	        	name.setText(arrayShortApps.get(position).getName());
		        packageName.setText(arrayShortApps.get(position).getPathName());
		        if (arrayShortApps.get(position).getIcon()!=null) {
		        	 image.setBackgroundDrawable(arrayShortApps.get(position).getIcon());
				}
		        else
		        {
		        	image.setBackgroundResource(arrayShortApps.get(position).getIntIcon());
		        }
		       
				}
	        if (infos!=null) {
	        	name.setText(infos.get(position).loadLabel(context.getPackageManager()));
		        packageName.setText(infos.get(position).activityInfo.packageName);
		        image.setBackgroundDrawable(infos.get(position).activityInfo.loadIcon(context.getPackageManager()));
			}
	        if (showbuttons==DELETE) {
	        	deleteImage.setVisibility(View.VISIBLE);
	        	deleteImage.setTag(position);
	        	deleteImage.setOnClickListener(new View.OnClickListener() {

	        	    @Override
	        	    public void onClick(View v) {
	        	        if(mClickListener != null)
	        	            mClickListener.onBtnClick((Integer) v.getTag());                
	        	    }
	        	});
			}
	        	        
	        return layout;
	}

}
