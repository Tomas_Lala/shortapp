package cz.lala.shortapp.setting.addapps;


import java.util.Collections;
import java.util.List;




import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppDatabase;
import cz.lala.shortapp.database.AppsChecker;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.ShortAppsAdapter;

public class Fr_InstalledApps extends SherlockListFragment implements OnItemClickListener{
	
	 List<ResolveInfo> infos ;


	 @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	inflater.inflate(R.menu.installedapps_menu, menu);
	super.onCreateOptionsMenu(menu, inflater);
} 
	 @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 setHasOptionsMenu(true);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public void onViewCreated(View view,Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		infos = AppsSaver.getInstalledAppsList();
		
		setListAdapter(new ShortAppsAdapter( 0,getSherlockActivity(),infos));
		getListView().setOnItemClickListener(this);
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (AppsSaver.getCountFavouriteAppsList()==10) {
			Toast.makeText(getSherlockActivity(), "Opps.. Too many ShortApps.", Toast.LENGTH_SHORT).show();
			return;
		}
		else if (AppsSaver.isAlreadyFavouriteApp(infos.get(position).activityInfo.packageName)) {
			Toast.makeText(getSherlockActivity(), "This one has already setted.", Toast.LENGTH_SHORT).show();
			return;
		}
		else{
			new AppsSaver(getActivity()).setFavouriteAppToDatabase(infos.get(position).activityInfo.packageName,
					infos.get(position).loadLabel(getSherlockActivity().getPackageManager()).toString(),
					AppDatabase.KIND_APPLIACATION,infos.get(position).loadIcon(getActivity().getPackageManager()),0,AppDatabase.NULL);
			Toast.makeText(getSherlockActivity(), infos.get(position).loadLabel(getSherlockActivity().getPackageManager()).toString()+" set as ShortApp", Toast.LENGTH_SHORT).show();
		}
		
	}

}
