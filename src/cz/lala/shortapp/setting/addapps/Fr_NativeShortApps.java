package cz.lala.shortapp.setting.addapps;


import java.util.ArrayList;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockListFragment;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppDatabase;
import cz.lala.shortapp.setting.ShortApp;
import cz.lala.shortapp.setting.ShortAppsAdapter;
import cz.lala.shortapp.setting.dialogs.Contact_Dialog;
import cz.lala.shortapp.setting.dialogs.Fb_Page_Dialog;
import cz.lala.shortapp.setting.dialogs.WebPage_Dialog;

public class Fr_NativeShortApps extends SherlockListFragment implements OnItemClickListener{

	private ArrayList<ShortApp> shortApps = new ArrayList<ShortApp>();
	private int[] icons = {R.drawable.ic_www_icon,R.drawable.ic_social_facebook_box_blue_icon,R.drawable.ic_contacts_icon,R.drawable.ic_social_facebook_box_blue_icon};
	private String[] names = {"Web Page","Facebook page","Contacts","Facebook group"};
	private String[] kinds = {AppDatabase.KIND_URL,AppDatabase.KIND_FB_PAGE,AppDatabase.KIND_CONTACT,AppDatabase.KIND_URL};
	DialogFragment newFragment;
//private StatusCallback mCallback;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		for (int i = 0; i < icons.length; i++) {
			shortApps.add(new ShortApp(names[i], "", kinds[i],null,icons[i]));
		}	
		setListAdapter(new ShortAppsAdapter(getSherlockActivity(),shortApps,0,null));
		getListView().setOnItemClickListener(this);
		
		/*	mCallback = new Session.StatusCallback() {
			 private RequestAsyncTask executeMeRequestAsync;
			  @Override
			    public void call(Session session, SessionState state, Exception exception) {
			        //onSessionStateChange(session, state, exception);
				  if (exception !=null) {
					System.out.println(exception);
				}
				   System.out.println("VBlaaa");
			        if (session.isOpened()) {
			            Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
			                @Override
			                public void onCompleted(GraphUser user, Response response) {
			                    if (user != null) {
			                       System.out.println("V poho");
			                    } else {
			                        // could not get user graph
			                    }
			                }
			            });
			        }
			    }
		   }; // the code you already have

		Session.OpenRequest request = new Session.OpenRequest(getSherlockActivity());
		request.setPermissions(Arrays.asList("user_friends"));
		request.setCallback(mCallback );

		// get active session
		Session mFacebookSession = Session.getActiveSession();
		if (mFacebookSession == null || mFacebookSession.isClosed()) 
		{
			System.out.println("Blue");
		    mFacebookSession = new Session(getSherlockActivity());
		    Session.setActiveSession(mFacebookSession);
		}
		mFacebookSession.openForRead(request);
		
		*/
	
		
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		switch (position) {
		case 0:
			  newFragment = new WebPage_Dialog(getSherlockActivity(),shortApps.get(position));
			   newFragment.show(getSherlockActivity().getSupportFragmentManager(), "createShortApp");
			break;
		case 1:
			newFragment = new Fb_Page_Dialog(getSherlockActivity(),shortApps.get(position));
			   newFragment.show(getSherlockActivity().getSupportFragmentManager(), "createShortApp");
			break;
		case 2:
			newFragment = new Contact_Dialog(getSherlockActivity(),shortApps.get(position));
			   newFragment.show(getSherlockActivity().getSupportFragmentManager(), "createShortApp");
			break;

		default:
			break;
		}
		
		
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newFragment!=null) {
			newFragment.dismiss();
		}	
	}
}
