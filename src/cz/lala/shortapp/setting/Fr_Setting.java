package cz.lala.shortapp.setting;




import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;
import cz.lala.shortapp.R;


public class Fr_Setting extends PreferenceFragment {
	
	public static String BOOT = "bootable_preference";
	public static String HIDE = "hide_preference";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

	}

}
