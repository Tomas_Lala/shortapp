package cz.lala.shortapp.setting.showapps;



import java.util.List;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppsChecker;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.Fr_Setting;
import cz.lala.shortapp.setting.ShortApp;
import cz.lala.shortapp.setting.ShortAppsAdapter;
import cz.lala.shortapp.setting.addapps.Fr_SelectShortApps;
import cz.lala.shortapp.setting.icons.Fr_SelectIcon;

public class Fr_MyShortApps extends SherlockFragment implements OnClickListener{
	
	public interface BtnClickListener {
	    public abstract void onBtnClick(int position);
	}
	
	private View view;
	private TextView numbers;
	private TextView addNewShortApp;
	private ListView listView;
	private List<ShortApp> packageInfos;
	private ShortAppsAdapter adapter;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fr_myshortapps, container,false);
		numbers = (TextView)view.findViewById(R.id.fr_myshortapps_number_of_shortapps);
		addNewShortApp = (TextView)view.findViewById(R.id.fr_myshortapps_add_first_shortapp);
		listView = (ListView)view.findViewById(R.id.fr_myshortapps_listview);
		packageInfos = AppsSaver.getFavouriteAppsList();
		addNewShortApp.setOnClickListener(this);
		setHasOptionsMenu(true);
		
		adapter = new ShortAppsAdapter(getSherlockActivity(), packageInfos,ShortAppsAdapter.DELETE, new BtnClickListener() {
			
			@Override
			public void onBtnClick(int position) {
				try {
					new AppsSaver(getSherlockActivity()).removeFavouriteApp(packageInfos.get(position).getPathName(), true, position);
					//packageInfos.remove(position);
					adapter.notifyDataSetChanged();
					numbers.setText(packageInfos.size() + "/10");
				} catch (Exception e) {
					e.printStackTrace();
				}
			
				
			}
		});
	
	if (packageInfos.size()!=0) {
		numbers.setText(packageInfos.size() + "/10");
		listView.setAdapter(adapter);
	}
	else{
		numbers.setText("0/10");
		addNewShortApp.setVisibility(View.VISIBLE);
	}
		return view;
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.main, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.main_add_shortapp:
			getFragmentManager().beginTransaction()
			.replace(R.id.container, new Fr_SelectShortApps(),"TO_B").addToBackStack("FROM_B_TO_A").commit();
			
			break;
		case R.id.main_setting :
			getFragmentManager().beginTransaction()
			.replace(R.id.container, new Fr_Setting(),"TO_B").addToBackStack("FROM_B_TO_A").commit();
			
			break;
		case R.id.main_icons :
			getFragmentManager().beginTransaction()
			.replace(R.id.container, new Fr_SelectIcon(),"TO_B").addToBackStack("FROM_B_TO_A").commit();
			
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onClick(View v) {
		
		getFragmentManager().beginTransaction()
		.replace(R.id.container, new Fr_SelectShortApps(),"TO_B").addToBackStack("FROM_B_TO_A").commit();
		
		
	}

}
