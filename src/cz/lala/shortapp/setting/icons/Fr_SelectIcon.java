package cz.lala.shortapp.setting.icons;




import java.util.ArrayList;
import java.util.HashMap;

import com.actionbarsherlock.app.SherlockListFragment;

import cz.lala.shortapp.R;
import cz.lala.shortapp.database.Icon_Database;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.test.IsolatedContext;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class Fr_SelectIcon extends SherlockListFragment implements OnItemClickListener{
	ArrayList<int[]> arrayList;
	int settedIcon;
	private SharedPreferences sharedPreferences;
	private SelectIcon_Adapter adapter;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		arrayList = new Icon_Database(getSherlockActivity()).getAllIcons();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getSherlockActivity());
		settedIcon = sharedPreferences.getInt(Icon_Database.ICONS, Icon_Database.DEFAULT);
		adapter = new SelectIcon_Adapter(getSherlockActivity(), settedIcon,arrayList);
		setListAdapter(adapter);
		getListView().setDividerHeight(0);
		getListView().setOnItemClickListener(this);
		
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		sharedPreferences.edit().putInt(Icon_Database.ICONS, position).commit();
		settedIcon = position;
		adapter.notifyDataSetChanged();
		
	}

}
