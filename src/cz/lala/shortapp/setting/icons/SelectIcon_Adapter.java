package cz.lala.shortapp.setting.icons;

import java.util.ArrayList;
import java.util.HashMap;

import cz.lala.shortapp.R;
import android.R.integer;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectIcon_Adapter extends BaseAdapter {

	ArrayList<int[]> arrayList;
	int selectPosition;
	private LayoutInflater inflater;
	
	public SelectIcon_Adapter(Context context,int selectPosition,ArrayList<int[]> arrayList) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.selectPosition = selectPosition;
		this.arrayList = arrayList;
	}
	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
	
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 View layout = convertView;
	        if (layout == null)
	        layout = inflater.inflate(R.layout.select_mainicon, parent,false);
	        TextView name = (TextView)layout.findViewById(R.id.select_mainicon_name);
	        ImageView selected = (ImageView)layout.findViewById(R.id.select_mainicon_selected);
	        ImageView iconLeft = (ImageView)layout.findViewById(R.id.select_mainicon_icon_left);
	        ImageView iconCenter = (ImageView)layout.findViewById(R.id.select_mainicon_icon_center);
	        ImageView iconRight = (ImageView)layout.findViewById(R.id.select_mainicon_icon_right);
	        if (selectPosition==position) {
				//selected.setText("SELECTED");
			}
	        name.setText("ICONS "+ position);
	        int[] icons= arrayList.get(position);
	        iconCenter.setBackgroundResource(icons[1]);
	        iconLeft.setBackgroundResource(icons[0]);
	        iconRight.setBackgroundResource(icons[2]);
	        
		return layout;
	}


}
