package cz.lala.shortapp.startup;

import cz.lala.shortapp.database.AppsChecker;
import cz.lala.shortapp.database.AppsChecker.OnAppsCheckerDone;
import cz.lala.shortapp.service.Windowservice;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.MainActivity;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class StartUpService extends Service implements OnAppsCheckerDone{

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onCreate() {
		// Zkontroluje a načte potřebné aplikace
		if (AppsSaver.getFavouriteAppsList()==null) {
			new AppsChecker(this, this).execute();
		}
		else{		
				onAppsCheckDone();
		}
		super.onCreate();
	}
	@Override
	public void onAppsCheckDone() {
		if (AppsSaver.getCountFavouriteAppsList()==0) {
			startActivity(new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
		}
		else
		{
			startService(new Intent(getApplicationContext(), Windowservice.class));
		}
		stopSelf();
	}

}
