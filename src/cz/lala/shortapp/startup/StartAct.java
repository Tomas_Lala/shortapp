package cz.lala.shortapp.startup;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import cz.lala.shortapp.database.AppsChecker;
import cz.lala.shortapp.database.AppsChecker.OnAppsCheckerDone;
import cz.lala.shortapp.service.Windowservice;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.MainActivity;



public class StartAct extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startService(new Intent(this, StartUpService.class));
		finish();
		
	}

}
