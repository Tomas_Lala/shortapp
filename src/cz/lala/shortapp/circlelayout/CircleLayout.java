package cz.lala.shortapp.circlelayout;

/*
 * Copyright 2013 Csaba Szugyiczki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import cz.lala.shortapp.R;
import cz.lala.shortapp.database.AppDatabase;

/**
 * 
 * @author Szugyi Creates a rotatable circle menu which can be parameterized by
 *         custom attributes. Handles touches and gestures to make the menu
 *         rotatable, and to make the menu items selectable and clickable.
 * 
 */
public class CircleLayout extends ViewGroup {

	
	public static final int RIGHT = 1;
	public static final int LEFT = 2;
	private int IS_ON_SIDE = 0;
	public static float SCALE_SIZE = 0.53f;
	private static int rotate_size = 0;
	private double startAngle;
	
	private int speed = 20;
	private float deceleration = 1 + (5f / speed);
	// The runnable of the current rotation
	private FlingRunnable actRunnable = null;
	
	// Event listeners
	private OnItemClickListener mOnItemClickListener = null;
	private OnItemSelectListener mOnItemSelectListener = null;
	// Background image
	private Bitmap imageOriginal, imageScaled;
	private Matrix matrix;

	private int mTappedViewsPostition = -1;
	private View mTappedView = null;
	private int selected = 0;

	// Child sizes
	private int mMaxChildWidth = 0;
	private int mMaxChildHeight = 0;
	private int childWidth = 0;
	private int childHeight = 0;

	// Sizes of the ViewGroup
	private int circleWidth, circleHeight;
	private int radius = 0;

	// Touch detection
	private GestureDetector mGestureDetector;
	// needed for detecting the inversed rotations
	private boolean[] quadrantTouched;

	// Settings of the ViewGroup
	private boolean allowRotating = true;
	private float angle = 90;
	private boolean isRotating = true;

	// The runnable of the current rotation
	//rivate FlingRunnable actRunnable = null;

	/**
	 * @param context
	 */
	public CircleLayout(Context context) {
		this(context, null);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public CircleLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public CircleLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	/**
	 * Initializes the ViewGroup and modifies it's default behavior by the
	 * passed attributes
	 * 
	 * @param attrs
	 *            the attributes used to modify default settings
	 */
	
////////////////////////////////////////////////////////////////////////
/////////////////////////// DO NOT TOUCH //////////////////////////////
//////////////////////////////////////////////////////////////////////	
	
	protected void init(AttributeSet attrs) {
		mGestureDetector = new GestureDetector(getContext(),
				new MyGestureListener());
		quadrantTouched = new boolean[] { false, false, false, false, false };

		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.Circle);

			// The angle where the first menu item will be drawn
			isRotating = a.getBoolean(R.styleable.Circle_isRotating, true);

			// If the menu is not rotating then it does not have to be centered
			// since it cannot be even moved

			a.recycle();

			// initialize the matrix only once
			if (matrix == null) {
				matrix = new Matrix();
			} else {
				// not needed, you can also post the matrix immediately to
				// restore the old state
				matrix.reset();
			}

			// Needed for the ViewGroup to be drawn
			setWillNotDraw(false);
		}
	}

	/**
	 * Returns the currently selected menu
	 * 
	 * @return the view which is currently the closest to the start position
	 */
	public View getSelectedItem() {
		return (selected >= 0) ? getChildAt(selected) : null;
	}

	@Override
	protected void onDraw(Canvas canvas) {		
		// the sizes of the ViewGroup
		circleHeight = getHeight();
		circleWidth = getWidth();
	}
	
	/**
	 * @return The angle of the unit circle with the image view's center
	 */
	private double getAngle(double xTouch, double yTouch) {
		double x = xTouch - (circleWidth / 2d);
		double y = circleHeight - yTouch - (circleHeight / 2d);

		switch (getQuadrant(x, y)) {
			case 1:
				return Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;

			case 2:
			case 3:
				return 180 - (Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI);

			case 4:
				return 360 + Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;

			default:
				// ignore, does not happen
				return 0;
		}
	}

	/**
	 * @return The selected quadrant.
	 */
	private static int getQuadrant(double x, double y) {
		if (x >= 0) {
			return y >= 0 ? 1 : 4;
		} else {
			return y >= 0 ? 2 : 3;
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isEnabled()) {
			if (isRotating) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:

						// reset the touched quadrants
						for (int i = 0; i < quadrantTouched.length; i++) {
							quadrantTouched[i] = false;
						}

						allowRotating = false;					
						startAngle = getAngle(event.getX(), event.getY());
						break;
					case MotionEvent.ACTION_MOVE:
						double currentAngle = getAngle(event.getX(),
								event.getY());
						rotateButtons((float) (startAngle - currentAngle));
						startAngle = currentAngle;
						break;
					case MotionEvent.ACTION_UP:
						allowRotating = true;
						break;
				}
			}

			// set the touched quadrant to true
			quadrantTouched[getQuadrant(event.getX() - (circleWidth / 2),
					circleHeight - event.getY() - (circleHeight / 2))] = true;
			mGestureDetector.onTouchEvent(event);
			return true;
		}
		return false;
	}

	private class MyGestureListener extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			if (!isRotating) {
				return false;
			}
			// get the quadrant of the start and the end of the fling
			int q1 = getQuadrant(e1.getX() - (circleWidth / 2), circleHeight
					- e1.getY() - (circleHeight / 2));
			int q2 = getQuadrant(e2.getX() - (circleWidth / 2), circleHeight
					- e2.getY() - (circleHeight / 2));

			// the inversed rotations
			if ((q1 == 2 && q2 == 2 && Math.abs(velocityX) < Math
					.abs(velocityY))
					|| (q1 == 3 && q2 == 3)
					|| (q1 == 1 && q2 == 3)
					|| (q1 == 4 && q2 == 4 && Math.abs(velocityX) > Math
							.abs(velocityY))
					|| ((q1 == 2 && q2 == 3) || (q1 == 3 && q2 == 2))
					|| ((q1 == 3 && q2 == 4) || (q1 == 4 && q2 == 3))
					|| (q1 == 2 && q2 == 4 && quadrantTouched[3])
					|| (q1 == 4 && q2 == 2 && quadrantTouched[3])) {
				
				CircleLayout.this.post(new FlingRunnable(-1
						* (velocityX + velocityY)/9));
			} else {
				// the normal rotation
				CircleLayout.this
						.post(new FlingRunnable((velocityX + velocityY)/9));
			}

			return true;

		
		}
			

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			mTappedViewsPostition = pointToPosition(e.getX(), e.getY());

			if (mTappedViewsPostition >= 0) {
				mTappedView = getChildAt(mTappedViewsPostition);
				mTappedView.setPressed(true);
			}
			else
			{
				return true;
			}

			if (mTappedView != null) {
				CircleImageView view = (CircleImageView) (mTappedView);
					mOnItemClickListener.onItemClick(mTappedView,
							view.getPosition(),IS_ON_SIDE);
				return true;
			}
			return super.onSingleTapUp(e);
		}
	}
	
	private class FlingRunnable implements Runnable {

		private float velocity;


		public FlingRunnable(float velocity) {
			this(velocity, true);
		}

		public FlingRunnable(float velocity, boolean isFirst) {
			this.velocity = velocity;


			if (Math.abs(velocity) > 1) {
				CircleLayout.this.actRunnable = this;
			}
		}

		public void run() {
					rotateButtons(velocity / speed);
					velocity /= deceleration;

					CircleLayout.this.post(this);
				}
			}
		

	/**
	 * A {@link Runnable} for animating the menu rotation.
	 */

	private int pointToPosition(float x, float y) {

		for (int i = 0; i < getChildCount(); i++) {

			View item = (View) getChildAt(i);
			if (item.getLeft() < x && item.getRight() > x & item.getTop() < y
					&& item.getBottom() > y) {
				return i;
			}

		}
		return -1;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		mMaxChildWidth = 0;
		mMaxChildHeight = 0;

		// Measure once to find the maximum child size.
		int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(
				MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.AT_MOST);
		int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(
				MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.AT_MOST);

		final int count = getChildCount();
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() == GONE) {
				continue;
			}

			child.measure(childWidthMeasureSpec, childHeightMeasureSpec);

			mMaxChildWidth = Math.max(mMaxChildWidth, child.getMeasuredWidth());
			mMaxChildHeight = Math.max(mMaxChildHeight,
					child.getMeasuredHeight());
		}

		// Measure again for each child to be exactly the same size.
		childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(mMaxChildWidth,
				MeasureSpec.EXACTLY);
		childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(mMaxChildHeight,
				MeasureSpec.EXACTLY);

		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() == GONE) {
				continue;
			}

			child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
		}

		setMeasuredDimension(resolveSize(mMaxChildWidth, widthMeasureSpec),
				resolveSize(mMaxChildHeight, heightMeasureSpec));
	}
	
////////////////////////////////////////////////////////////////////////
/////////////////////////// LALAS CODE ////////////////////////////////
//////////////////////////////////////////////////////////////////////
	

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		int layoutWidth = r - l;
		int layoutHeight = b - t;
		boolean textsetted = false;

		// Laying out the child views
		final int childCount = getChildCount();
		int left, top;
		radius = (layoutWidth <= layoutHeight) ? layoutWidth / 3
				: layoutHeight / 3;

		childWidth = (int) (radius * SCALE_SIZE);
		childHeight = (int) (radius * SCALE_SIZE);

		float angleDelay = 36;
		if (childCount>5) {
			isRotating = true;
		}
		else
		{
			isRotating = false;
		}
		if ((rotate_size!=0)&&(allowRotating)) {
			angle = rotate_size;
			rotate_size = 0;
		}

		for (int i = 0; i < childCount; i++) {
			 CircleImageView child = null;
			if (IS_ON_SIDE==RIGHT) {				
			child = (CircleImageView) getChildAt(i);
			child.setPosition(i);
			}
			else{
			child = (CircleImageView) getChildAt(childCount-1-i);
			child.setPosition(childCount-1-i);
			}
			if (child.getVisibility() == GONE) {
				continue;
			}
			
			if (angle > 360) {
				angle -= 360;
			} else {
				if (angle < 0) {
					angle +=360;
				}
			}
			child.setAngle(angle);

			left = Math
					.round((float) (((layoutWidth / 2) - childWidth / 2) + radius
							* Math.cos(Math.toRadians(angle))));
			top = Math
					.round((float) (((layoutHeight / 2) - childHeight / 2) + radius
							* Math.sin(Math.toRadians(angle))));

			child.layout(left, top, left + childWidth, top + childHeight);
			angle += angleDelay;
		}
		angle-=36*getChildCount();
	}

	/**
	 * Rotate the buttons.
	 * 
	 * @param degrees
	 *            The degrees, the menu items should get rotated.
	 */
	private void rotateButtons(float degrees) {
		int left, top, childCount = getChildCount();
		boolean textsetted = false;
		float angleDelay = 36;
			angle += degrees;
		if (angle > 360) {
			angle -= 360;
		} else {
			if (angle < 0) {
				angle +=360;
			}
		}		
		for (int i = 0; i < childCount; i++) {
			if (angle > 360) {
				angle -= 360;
			} else {
				if ((angle < 0)) {
					angle +=360;
				}
			}
			CircleImageView child = null;
			if (IS_ON_SIDE==RIGHT) {
				child = (CircleImageView) getChildAt(i);
				if ((angle>=(180-angleDelay/2))&&(angle<=(180+angleDelay/2))&&(!child.getKind().equals(AppDatabase.KIND_APPLIACATION))) {
					textsetted = true;
					mOnItemSelectListener.onItemSelect(i, IS_ON_SIDE);
				}
			}
			else
			{
				child = (CircleImageView) getChildAt(getChildCount()-1-i);
				if (((angle>=(360-(angleDelay/2)))||(angle<=(angleDelay/2)))&&(!child.getKind().equals(AppDatabase.KIND_APPLIACATION))) {
					textsetted = true;
					mOnItemSelectListener.onItemSelect(getChildCount()-1-i,IS_ON_SIDE);
				}
			}
			if (child.getVisibility() == GONE) {
				continue;
			}
			left = Math
					.round((float) (((circleWidth / 2) - childWidth / 2) + radius
							* Math.cos(Math.toRadians(angle))));
			top = Math
					.round((float) (((circleHeight / 2) - childHeight / 2) + radius
							* Math.sin(Math.toRadians(angle))));		
			
			child.setAngle(angle);
			child.layout(left, top, left + childWidth, top + childHeight);
			angle += angleDelay;
		}
		if (!textsetted) {
			mOnItemSelectListener.onItemSelect(11, IS_ON_SIDE);
		}
		angle-=36*getChildCount();
	
	}

	public void rotateToRight() {
		IS_ON_SIDE = LEFT;
		rotate_size=180;
		switch (getChildCount()) {
		case 1:
			rotate_size=72;
			break;
		case 2:
			rotate_size=36;
			break;
		case 3:
			rotate_size=360;
			break;
		case 4:
			rotate_size=9*36;
			break;
		case 5:
			rotate_size=8*36;
			break;
	case 6:
		rotate_size=7*36;
			break;
	case 7:
		rotate_size=6*36;
		break;
	case 8:
		rotate_size=180;
		break;
	case 9:
		rotate_size=144;
		break;
	case 10:
		rotate_size=108;
		break;

		default:
			break;
		}	

	}
	
	public void rotateToLeft() {
		IS_ON_SIDE = RIGHT;
		rotate_size=108;
	}



	

////////////////////////////////////////////////////////////////////////
/////////////////////////// INTERFACES ////////////////////////////////
//////////////////////////////////////////////////////////////////////

	public interface OnItemSelectListener {
		void onItemSelect(int position, int iS_ON_SIDE);
	}

	public void setOnItemSelectListener(OnItemSelectListener mOnItemSelectListener) {
		this.mOnItemSelectListener = mOnItemSelectListener;
	}
	public interface OnItemClickListener {
		void onItemClick(View view, int position, int side);
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.mOnItemClickListener = onItemClickListener;
	}

}