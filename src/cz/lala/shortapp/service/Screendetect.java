package cz.lala.shortapp.service;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

public class Screendetect extends RelativeLayout {

	private String FLAG = "LegacyFSDetect";
			
	private OnFullScreenListener OnFullScreenListener;

	private Context mContext;
	
	private boolean FS_Bool;

	private boolean mAttached;

	public Screendetect(Context context) {
		super(context);
		mContext = context;
	}

	public Screendetect(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		mContext = context;
	}

	public Screendetect(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
		mContext = context;
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (!mAttached) {
			mAttached = true;
		}
	}
	
	// this does the magic
	@Override
	protected void onLayout (boolean changed, int l, int t, int r, int b){
		Log.d(FLAG, "changed - "+changed);
		// TODO this.setBackgroundColor(Color.argb(80, 0, 255, 255));
		if (changed){
			Log.d(FLAG, "screen - " + t + " x " + this.getHeight());
			 onFSChanged();
		}
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			mAttached = false;
		}
	}
	
	public boolean getFS()
	{
		return FS_Bool;
	}
	
	// do some math... and update the listeners ect ect
	
	private void onFSChanged()
	{
		
		Log.d(FLAG, "FS changed");
		
		if(OnFullScreenListener != null){			 
				Rect dim = new Rect();
				getWindowVisibleDisplayFrame(dim);
				Log.d(FLAG, dim.bottom + " "+dim.top + " "+dim.right+ " "+dim.left);
			 if( dim.top == 0){
				Log.d(FLAG, "screen FS true - " + dim.top);
				 FS_Bool = true;
			 } else {
				Log.d(FLAG, "screen FS false  - " + dim.top);
				 FS_Bool = false;
			 }
			
			 OnFullScreenListener.fsChanged(getFS());
		}
	}

	
	public void setOnFullScreenListener(OnFullScreenListener listener)
	{
		this.OnFullScreenListener = listener;
	}
	
	public interface OnFullScreenListener
	{
		public void fsChanged(boolean FS_Bool);
	}

}