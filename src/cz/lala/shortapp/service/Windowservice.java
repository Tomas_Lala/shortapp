package cz.lala.shortapp.service;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.splunk.mint.Mint;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cz.lala.shortapp.R;
import cz.lala.shortapp.circlelayout.CircleImageView;
import cz.lala.shortapp.circlelayout.CircleLayout;
import cz.lala.shortapp.circlelayout.CircleLayout.OnItemClickListener;
import cz.lala.shortapp.circlelayout.CircleLayout.OnItemSelectListener;
import cz.lala.shortapp.database.AppDatabase;
import cz.lala.shortapp.database.AppsChecker;
import cz.lala.shortapp.database.AppsChecker.OnAppsCheckerDone;
import cz.lala.shortapp.database.Icon_Database;
import cz.lala.shortapp.service.Screendetect.OnFullScreenListener;
import cz.lala.shortapp.setting.AppsSaver;
import cz.lala.shortapp.setting.Fr_Setting;
import cz.lala.shortapp.setting.MainActivity;
import cz.lala.shortapp.setting.ShortApp;
import cz.lala.shortapp.setting.icons.Fr_SelectIcon;
import cz.lala.shortapp.setting.icons.SelectIcon_Adapter;


public class Windowservice extends Service implements OnItemClickListener, OnItemSelectListener, OnAppsCheckerDone, AnimationListener{
	
	
	private static final int ANIMATION_FRAME_RATE 			= 27;	// Animation frame rate per second.
	private static final float ANIMATION_DELETEAREA_HEIGHT = 0.85f;
	public static final int ANIMATION_RIGHT_OR_LEFT_SPEED = 300;
	private static final int ANIMATION_BOTTOM_SPEED = 0;
	
	private static final int ANIMATION_ON_UP_CHATHEAD = 0;
	private static final int ANIMATION_MOVE_BIN = 1;
	private static final int ANIMATION_SHOW = 2;
	private static final int ANIMATION_HIDE = 3;
	
	private WindowManager 				mWindowManager;			// Reference to the window
	private WindowManager.LayoutParams 	mMainIconLayoutParams;		// Parameters of the root layout
	private WindowManager.LayoutParams 	mDeleteAreaLayoutParams;
	private WindowManager.LayoutParams 	mAppChooserLayoutParams;
	// Variables that control drag
	private int mStartDragX;
	//private int mStartDragY; // Unused as yet
	private int mPrevDragX;
	private int mPrevDragY;
	
	//Šířka Displeje
	private int mDisplayWidth;
	private int mDisplayHeight;
		
	
	
	// Controls for animations
		private Timer 					mMainIconAnimationTimer;
		private Timer 					mDeleteAreaAnimationTimer;
		private TrayAnimationTimerTask 	mTrayTimerTask;
		private Handler 				mAnimationHandler = new Handler();

	// 	
	private boolean isMainIconMoved = false;
	private boolean isMainIconInDeleteArea = false;
	private boolean isDeleteAreaHidden = true;
	private boolean isAppchooserShown = false;
	private boolean areViewsDeleted = false;
	private boolean isMainIconStopped = false;
	
	
	private LayoutInflater layoutInflater;
	private ImageView deleteArea;
	private ImageView setting;
	private RelativeLayout mAppChooserRootLayout;
	private CircleLayout mAppChooserCircleLayout;
	
	
	private LinearLayout mainIconlinearlayout;
	private ImageView mainIconImage;
	private int[] mainicons;
	
	
	private Animation animationMainIcon;
	private ObjectAnimator animationAppChooset;
	private List<ShortApp> mFavouriteApps;
	private TranslateAnimation animation;
	private TextView textName;
	private SharedPreferences sharedPreferences;
	private Screendetect mDetector;
	
	
	
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	@Override 
	public void onCreate() {
		super.onCreate();
		Mint.initAndStartSession(this, "9efb25f9");
	
		createAll();
    
	  }
	
	
	private void createAll()
	{
		
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		if (layoutInflater==null) {
		layoutInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		} 	
	 	if (mainIconlinearlayout==null) {
	 		 mainIconlinearlayout = (LinearLayout)layoutInflater.inflate(R.layout.main_icon, null);
	 	}
	 	if (mFavouriteApps==null) {
	 		mFavouriteApps = AppsSaver.getFavouriteAppsList();
	 	}
	 	if (mainicons==null) {
			int icons = sharedPreferences.getInt(Icon_Database.ICONS, 0);
			mainicons = new Icon_Database(this).getIcons(icons);
		}
	 	if (deleteArea==null) {
	 		 deleteArea = new ImageView(this);
	 		 deleteArea.setImageResource(R.drawable.ic_garbage_bin_icon);
	 	}
	 	if (mainIconImage==null) {
	 		 mainIconImage = (ImageView)mainIconlinearlayout.findViewById(R.id.main_icon_image);
	 		 mainIconImage.setImageResource(mainicons[0]);
	 	}
	 	if (mAppChooserRootLayout==null) {
	 		mAppChooserRootLayout = (RelativeLayout)layoutInflater.inflate(R.layout.appchooser, null);
	 		mAppChooserCircleLayout  = (CircleLayout)mAppChooserRootLayout.findViewById(R.id.appchooser_circlelayout);
			textName = (TextView)mAppChooserRootLayout.findViewById(R.id.appchooser_textview);
			setting = (ImageView)mAppChooserRootLayout.findViewById(R.id.appchooser_setting);
	 	} 	
	 	if (mWindowManager!=null) {
	 		try {
				mWindowManager.removeView(mAppChooserRootLayout);
			} catch (Exception e) {}
			try {
				mWindowManager.removeView(deleteArea);
			} catch (Exception e) {}
			try {
				mWindowManager.removeView(mainIconlinearlayout);
			} catch (Exception e) {}
		
		}
	 	else
	 	{
	 		mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
	 	}
	    	  	

	    
	    if (AppsSaver.getFavouriteAppsList()==null) {
			new AppsChecker(this, this).execute();
		}
	    else
	    {	    	
	    	setAllParams();
	    }  
	}
	
	
	
	private void setAllParams()
	{		
		
		  	getDisplaySizes();
		    setSettingButton();	   
		    setOnTouchAndOnClickMainIcon();
		    setAppsInCircle();
		
		     
		    setMainIconLayoutParams();
		    setDeleteAreaLayoutParams();
		    setAppChooserLayoutParams();
		    setScreenDecektorParams();
	}
	

	private void setScreenDecektorParams() {
		mDetector = new Screendetect(this);
		WindowManager.LayoutParams paramtester = new WindowManager.LayoutParams(
				1,
				WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
				PixelFormat.TRANSPARENT);
		// Fix for the canot click install on sideloaded apps bug
		// Dont use fill screen, set to one side with a few pixels wide
		// 
		paramtester.gravity = Gravity.RIGHT | Gravity.TOP; 
		try {
			mWindowManager.addView(mDetector, paramtester);
		} catch (Exception e) {}
		

		mDetector.setOnFullScreenListener(new OnFullScreenListener() {
			@Override
			public void fsChanged(boolean FS_Bool) {
				if (FS_Bool) {
					try {
						areViewsDeleted=true;
						mWindowManager.removeView(mainIconlinearlayout);
						mWindowManager.removeView(deleteArea);
					} catch (Exception e) {}
				} else if (!FS_Bool) {
					try {	
						if (areViewsDeleted) {
							areViewsDeleted=false;
							mWindowManager.addView(mAppChooserRootLayout, mAppChooserLayoutParams);
							mWindowManager.addView(mainIconlinearlayout, mMainIconLayoutParams);
							
						}
						
					} catch (Exception e) {createAll();}
				}
			}
		});
	}


	@Override
	public void onAppsCheckDone() {
		mFavouriteApps = AppsSaver.getFavouriteAppsList();
		setAllParams();
		
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		  if (sharedPreferences.getBoolean(Fr_Setting.HIDE, false)) {
			    Intent home_intent = new Intent("android.intent.action.MAIN");
			    home_intent.addCategory("android.intent.category.HOME");
			    home_intent.addCategory("android.intent.category.DEFAULT");
			    final Handler handler = new Handler();
			  final String nameOfLauncherPkg= home_intent.resolveActivity(getPackageManager()).getPackageName();
			 Timer timer  =  new Timer();
			    timer.scheduleAtFixedRate(new TimerTask() {
			    	

					public void run() 
			         {
						if (!areViewsDeleted) {
			    		 final ActivityManager activityManager  =  (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
			             if (!activityManager.getRunningTasks(1).get(0).topActivity.getPackageName().equals(nameOfLauncherPkg)&&!isMainIconStopped) {
			            	 isMainIconStopped = true;
								
							
							handler.post( new Runnable() {
								
								@Override
								public void run() {								
									moveMainIcon(ANIMATION_HIDE);
								}
							});
						
						}
			             if (activityManager.getRunningTasks(1).get(0).topActivity.getPackageName().equals(nameOfLauncherPkg)&&isMainIconStopped&&!isAppchooserShown) {
			            	 isMainIconStopped = false;
								handler.post( new Runnable() {
									
									@Override
									public void run() {
										moveMainIcon(ANIMATION_SHOW);
									}
								});
								 
							}
						}
			         }
			    }, 1000, 2600); 
			}
		
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	//////////////////////////////////////////////////////
   /////////////// MAIN ICON ////////////////////////////
  //////////////////////////////////////////////////////

	private void  setMainIconLayoutParams()
	{
		  mMainIconLayoutParams = new WindowManager.LayoutParams(
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.TYPE_PHONE,
			        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			        | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR, 
			        PixelFormat.TRANSLUCENT);

			    mMainIconLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
			    mMainIconLayoutParams.x = 0;
			    mMainIconLayoutParams.y = 100;
				try {
					 mWindowManager.addView(mainIconlinearlayout, mMainIconLayoutParams);
				} catch (Exception e) {}
					    
			    moveMainIcon(ANIMATION_SHOW);
	}
	
	// Set to Main Icon responding for Click and Touch
	private void setOnTouchAndOnClickMainIcon() {
		mainIconlinearlayout.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int x = (int)event.getRawX();
				int y = (int)event.getRawY();
				if (!isMainIconStopped) {		
				 switch (event.getAction()) {
			      case MotionEvent.ACTION_DOWN:
			    	// Cancel any currently running animations/automatic tray movements.
						if (mTrayTimerTask!=null){
							mTrayTimerTask.cancel();
							mMainIconAnimationTimer.cancel();
						}				
						// Store the start points
						mStartDragX = x;
						mPrevDragX = x;
						mPrevDragY = y;
						break;
			    
			      case MotionEvent.ACTION_MOVE:						
						// Calculate position of the whole tray according to the drag, and update layout.					    	  
			    	  	mainIconImage.setImageResource(mainicons[1]);
						float deltaX = x-mPrevDragX;
						float deltaY = y-mPrevDragY;
						mMainIconLayoutParams.x += deltaX;
						mMainIconLayoutParams.y += deltaY;
						mPrevDragX = x;
						mPrevDragY = y;
						isMainIconMoved = true;
						if(mMainIconLayoutParams.x<0)mMainIconLayoutParams.x = 0;
						if(mMainIconLayoutParams.x>mDisplayWidth)mMainIconLayoutParams.x = mDisplayWidth;
						if(mMainIconLayoutParams.y>mDisplayHeight)mMainIconLayoutParams.y = mDisplayHeight;
						if(mMainIconLayoutParams.y<0)mMainIconLayoutParams.y = 0;
						
							mWindowManager.updateViewLayout(mainIconlinearlayout, mMainIconLayoutParams);
						
						
							
			    	  		if (isDeleteAreaHidden) {
			    	  		// Ukazat DeleteArea
								isDeleteAreaHidden = false;		
								moveDeleteArea();
							}
			    	  		if (!isDeleteAreaHidden) {
			    	  			OnHoverDeleteArea();
							}
											
						
						break;
					
			      case MotionEvent.ACTION_UP:
			      case MotionEvent.ACTION_CANCEL:
			    	  	// Ukončit Service
						if (isMainIconInDeleteArea) {
							Windowservice.this.stopSelf();
							return false;
						}	
						
						// Schovat DeleteText	
			    	  	if (!isDeleteAreaHidden) {
			    	  		isDeleteAreaHidden = true;
			    	  		moveDeleteArea();
						}
			    				    	 
			    	  
						
			    	  	if (!isMainIconMoved&&!isMainIconStopped) {
			    	  		onMainIconClick();	   		  	
							break;
			    	  	}
			    	  
			    	  		isMainIconMoved = false;
							mTrayTimerTask = new TrayAnimationTimerTask(ANIMATION_ON_UP_CHATHEAD);
							mMainIconAnimationTimer = new Timer();
							mMainIconAnimationTimer.schedule(mTrayTimerTask, 30, ANIMATION_FRAME_RATE);
							
							if((mDisplayWidth-mainIconlinearlayout.getWidth())/2<mMainIconLayoutParams.x){
				    	  		mainIconImage.setImageResource(mainicons[2]);
				    	  	}
				    	  	else
				    	  	{
				    	  		mainIconImage.setImageResource(mainicons[0]);
				    	  	}
							break;
				       
			    }
				}
			    return false;
			}
		});		
	}
	
	// MainIcon Clicked
	public void onMainIconClick()
	{		
				
			if (((mMainIconLayoutParams.y+(mainIconlinearlayout.getHeight()/2))+(mAppChooserCircleLayout.getHeight()/2))>mDisplayHeight) {
					mAppChooserCircleLayout.setY(mDisplayHeight-(mAppChooserCircleLayout.getHeight()));
			}
			else if (((mMainIconLayoutParams.y+(mainIconlinearlayout.getHeight()/2))-(mAppChooserCircleLayout.getHeight()/2))<0) {
				mAppChooserCircleLayout.setY(0);
			} else {
				mAppChooserCircleLayout.setY((mMainIconLayoutParams.y+(mainIconlinearlayout.getHeight()/2))-(mAppChooserCircleLayout.getHeight()/2));
			}
			setting.setY(mAppChooserCircleLayout.getY()-(setting.getHeight()/2)+(mAppChooserCircleLayout.getHeight()/2));
			mAppChooserRootLayout.setVisibility(View.VISIBLE);
			moveMainIcon(ANIMATION_HIDE);
			moveAppChooser();
	}

	private void moveMainIcon(int animationKind) {
		try {
			
		
		switch (animationKind) {
		case ANIMATION_SHOW:
			mainIconlinearlayout.setVisibility(View.VISIBLE);
			if (mMainIconLayoutParams.x>(mDisplayWidth/2)) {
				isMainIconStopped = false;
				animationMainIcon = AnimationUtils.loadAnimation(this,
		                R.anim.move_from_right_to_center);
			}
			else
			{	
				isMainIconStopped = false;
				animationMainIcon = AnimationUtils.loadAnimation(this,
		                R.anim.move_from_left_to_center);
			}
			break;
		case ANIMATION_HIDE:
			if (mMainIconLayoutParams.x>(mDisplayWidth/2)) {
				isMainIconStopped = true;
				animationMainIcon = AnimationUtils.loadAnimation(this,
		                R.anim.move_from_center_to_right);
			}
			else
			{
				isMainIconStopped = true;
				animationMainIcon = AnimationUtils.loadAnimation(this,
		                R.anim.move_from_center_to_left);
			}
			break;
		}
		animationMainIcon.setAnimationListener(this);		
		mainIconImage.startAnimation(animationMainIcon);
		} catch (NullPointerException e) {
				createAll();
		}
		
	}
	
	//////////////////////////////////////////////////////
   //////////////// DELETE AREA /////////////////////////
  //////////////////////////////////////////////////////
	
	private void setDeleteAreaLayoutParams() {
		mDeleteAreaLayoutParams = new WindowManager.LayoutParams(
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.WRAP_CONTENT,
			        WindowManager.LayoutParams.TYPE_PHONE,
			      WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
			        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, 
			        PixelFormat.TRANSLUCENT);
		mDeleteAreaLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;

		mDeleteAreaLayoutParams.y = mDisplayHeight;
		BitmapDrawable bd=(BitmapDrawable) this.getResources().getDrawable(R.drawable.ic_garbage_bin_icon);
		int width=bd.getBitmap().getWidth();
		mDeleteAreaLayoutParams.x = (mDisplayWidth-width)/2;
		try {
			mWindowManager.addView(deleteArea, mDeleteAreaLayoutParams);
		} catch (Exception e) {}
			
		
	}
	
	private void OnHoverDeleteArea() {
		int mainIconCenterx= mMainIconLayoutParams.x + (mainIconlinearlayout.getWidth()/2);
		int mainIconCentery= mMainIconLayoutParams.y + (mainIconlinearlayout.getHeight()/2);
		if((mainIconCenterx>mDeleteAreaLayoutParams.x)&&(mainIconCenterx<mDeleteAreaLayoutParams.x+deleteArea.getWidth())
				&&(mainIconCentery>mDeleteAreaLayoutParams.y)&&(mainIconCentery<mDeleteAreaLayoutParams.y+deleteArea.getHeight())){
			if (!isMainIconInDeleteArea) {
				isMainIconInDeleteArea = true;
				deleteArea.setImageResource(R.drawable.ic_garbage_bin_icon_open);
			}
			
		}
		else
		{
			if (isMainIconInDeleteArea) {
				isMainIconInDeleteArea = false;
				deleteArea.setImageResource(R.drawable.ic_garbage_bin_icon);
			}
		}
		
	}

    protected void moveDeleteArea() {
			    mTrayTimerTask = new TrayAnimationTimerTask(ANIMATION_MOVE_BIN);
			    mDeleteAreaAnimationTimer = new Timer();
			    mDeleteAreaAnimationTimer.schedule(mTrayTimerTask, 30, ANIMATION_FRAME_RATE);
		
	}

    
	//////////////////////////////////////////////////////
   /////////////// APPCHOOSER ///////////////////////////
  //////////////////////////////////////////////////////
    
	private void setAppChooserLayoutParams() {
		  mAppChooserLayoutParams = new WindowManager.LayoutParams(
			        WindowManager.LayoutParams.MATCH_PARENT,
			        WindowManager.LayoutParams.MATCH_PARENT,
			        WindowManager.LayoutParams.TYPE_PHONE,
				  	 WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			        | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
			        WindowManager.LayoutParams.FLAG_DIM_BEHIND,
			        PixelFormat.TRANSLUCENT); 
		  mAppChooserLayoutParams.dimAmount = 0.7f;
			try {
				  mWindowManager.addView(mAppChooserRootLayout, mAppChooserLayoutParams);
			} catch (Exception e) {}

		  
		  mAppChooserRootLayout.setOnClickListener( new OnClickListener() {
			
			

			@Override
			public void onClick(View v) {
				isAppchooserShown = false;
				mAppChooserRootLayout.setVisibility(View.GONE);
				textName.setVisibility(View.INVISIBLE);
				moveMainIcon(ANIMATION_SHOW);
			}
		});
		  
		 
		
	}

	private void moveAppChooser() {
		isAppchooserShown = true;
		isMainIconStopped = true;
			if (mMainIconLayoutParams.x>(mDisplayWidth/2)) {
				  mAppChooserCircleLayout.rotateToLeft();
				animationAppChooset= ObjectAnimator.ofFloat(mAppChooserCircleLayout, "x",mDisplayWidth,mDisplayWidth-mAppChooserCircleLayout.getWidth()/2 );
				animationAppChooset.setDuration(400);
				animationAppChooset.start();
				animationAppChooset= ObjectAnimator.ofFloat(setting, "x",mDisplayWidth,mDisplayWidth-setting.getWidth()*0.75f);
				animationAppChooset.setDuration(500);
				animationAppChooset.start();
				
			}
			else
			{
				mAppChooserCircleLayout.rotateToRight();
				animationAppChooset= ObjectAnimator.ofFloat(mAppChooserCircleLayout, "x",-mAppChooserCircleLayout.getWidth(),-mAppChooserCircleLayout.getWidth()/2 );
				animationAppChooset.setDuration(400);
				animationAppChooset.start();
				animationAppChooset= ObjectAnimator.ofFloat(setting, "x",-setting.getWidth(),-setting.getWidth()*0.25f);
				animationAppChooset.setDuration(500);
				animationAppChooset.start();
			}
	}

	private void setSettingButton() {
		setting.setBackgroundResource(R.drawable.ic_setting);
		setting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				stopSelf();
				startActivity(new Intent(Windowservice.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));	
			}
		});
		
	}

    private void setAppsInCircle()
    {
    	try {
    		mAppChooserCircleLayout.removeAllViews();
		} catch (Exception e) {}	 	
    	mAppChooserCircleLayout.setOnItemClickListener(this);
    	mAppChooserCircleLayout.setOnItemSelectListener(this);
 	    int size = (int)(((BitmapDrawable)this.getResources().getDrawable(R.drawable.ic_launcher)).getBitmap().getHeight());
 	    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)(size*5.5), (int)(size*5.5));
 	    mAppChooserCircleLayout.setLayoutParams(params);
 	    for (int i = 0; i < mFavouriteApps.size(); i++) {
 	    	CircleImageView view = new CircleImageView(this);
 	    	if (mFavouriteApps.get(i).getIcon()!=null) {
 	    		if (android.os.Build.VERSION.SDK_INT >= 16){
 	    			view.setBackground(mFavouriteApps.get(i).getIcon());
 	    		}
 	    		else{
 	    			view.setBackgroundDrawable(mFavouriteApps.get(i).getIcon());
 	    		}	
 	    	}
 	    	else
 	    	{
 	    		view.setBackgroundResource(mFavouriteApps.get(i).getIntIcon());	
 	    	}
 			view.setKind(mFavouriteApps.get(i).getKind());
  			mAppChooserCircleLayout.addView(view);
 			
 		}
    }
	
    @Override
	public void onItemClick(View view, int position, int side) {
    	try{	
    	if (mFavouriteApps.get(position).getKind().equals(AppDatabase.KIND_APPLIACATION)) {
    		
    	    		Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage(mFavouriteApps.get(position).getPathName());
    	    		this.startActivity(LaunchIntent);	
    		}
    		if (mFavouriteApps.get(position).getKind().equals(AppDatabase.KIND_URL)) {
    		
    				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mFavouriteApps.get(position).getPathName())).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				this.startActivity(browserIntent);
			}
    		if (mFavouriteApps.get(position).getKind().equals(AppDatabase.KIND_CONTACT)) {    	 
    			
    	        	Intent intent = new Intent(Intent.ACTION_VIEW).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	        	System.out.println(mFavouriteApps.get(position).getPathName());
    	   		    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(mFavouriteApps.get(position).getPathName().replace(AppDatabase.KIND_CONTACT, "")));
    	   		    intent.setData(uri);
    	   		    this.startActivity(intent);    
    		}
    		
    		mAppChooserRootLayout.performClick();
    		} catch (Exception e) {
				e.printStackTrace();
				new AppsSaver(this).removeFavouriteApp(mFavouriteApps.get(position).getPathName(), true, position);
				mAppChooserCircleLayout.removeViewAt(position);
				mAppChooserCircleLayout.requestLayout();
			}
		}

	@Override
	public void onItemSelect(int position, int isOnSide) {
		if (position==11) {
			textName.setVisibility(View.INVISIBLE);
			return;
		}
		textName.setText(mFavouriteApps.get(position).getName());
		textName.setY(mAppChooserCircleLayout.getY()+mAppChooserCircleLayout.getHeight()/2);
		if (isOnSide==CircleLayout.LEFT) {	
			textName.setX(mAppChooserCircleLayout.getWidth()/2);
		}
		if (isOnSide==CircleLayout.RIGHT){
			textName.setX(mDisplayWidth-mAppChooserCircleLayout.getWidth()/2-textName.getWidth());
		}
		textName.setVisibility(View.VISIBLE);
		
	}
    
    //////////////////////////////////////////////////////
   /////////////// OTHER ////////////////////////////////
  //////////////////////////////////////////////////////
    
	
	// The phone orientation has changed. Update the widget's position.
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		getDisplaySizes();
		try{
			if (!areViewsDeleted) {
				
			
			isAppchooserShown = false;
			mAppChooserRootLayout.setVisibility(View.GONE);
			textName.setVisibility(View.INVISIBLE);
			mMainIconLayoutParams.x = 0;
			mMainIconLayoutParams.y = mDisplayHeight/2;
			mWindowManager.updateViewLayout(mainIconlinearlayout, mMainIconLayoutParams);
			mDeleteAreaLayoutParams.x = mDisplayWidth/2-(deleteArea.getWidth()/2);
			mDeleteAreaLayoutParams.y = mDisplayHeight;
			mWindowManager.updateViewLayout(deleteArea, mDeleteAreaLayoutParams);
			}
		}
		catch(Exception e)
		{createAll();}
		super.onConfigurationChanged(newConfig);
	}
	
	
	// Get width and height of display
	public void getDisplaySizes() {
		 int result = 0;
		   int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		   if (resourceId > 0) {
		      result = getResources().getDimensionPixelSize(resourceId);
		   }
		 mDisplayWidth = Windowservice.this.getResources().getDisplayMetrics().widthPixels;
		 mDisplayHeight= Windowservice.this.getResources().getDisplayMetrics().heightPixels-result;
	}
	
	@Override
	public void onAnimationStart(Animation animation) {		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if (isMainIconStopped) {
			mainIconlinearlayout.setVisibility(View.GONE);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	
	// Timer for animation/automatic movement of the tray.
	private class TrayAnimationTimerTask extends TimerTask{
			// Druh animace
			private final int animationKind	;
			public TrayAnimationTimerTask(int animationKind){
				super();
				this.animationKind = animationKind;
				// Setup destination coordinates based on the tray state. 
				
			}
			// This function is called after every frame.
			@Override
			public void run() {
				
				// handler is used to run the function on main UI thread in order to
				// access the layouts and UI elements.
				mAnimationHandler.post(new Runnable() {
					@Override
					public void run() {
						if (areViewsDeleted) {
							return;
						}
						switch (animationKind) {
						
						case ANIMATION_ON_UP_CHATHEAD:
							if((mDisplayWidth-mainIconlinearlayout.getWidth())/2<mMainIconLayoutParams.x){	
								mMainIconLayoutParams.x =(mDisplayWidth-mainIconlinearlayout.getWidth())-((2*((mDisplayWidth-mainIconlinearlayout.getWidth())-mMainIconLayoutParams.x))/3);	
								mWindowManager.updateViewLayout(mainIconlinearlayout, mMainIconLayoutParams);	
								// Cancel animation when the destination is reached
								if (mDisplayWidth-mainIconlinearlayout.getWidth()<=mMainIconLayoutParams.x){
									TrayAnimationTimerTask.this.cancel();
									mMainIconAnimationTimer.cancel();
								}
							}
								else{
								mMainIconLayoutParams.x = (2*(mMainIconLayoutParams.x))/3;					
								mWindowManager.updateViewLayout(mainIconlinearlayout, mMainIconLayoutParams);
								// Cancel animation when the destination is reached
								if (mMainIconLayoutParams.x<=0){
									TrayAnimationTimerTask.this.cancel();
									mMainIconAnimationTimer.cancel();
								}
							}
							break;
							case ANIMATION_MOVE_BIN:
								if (isDeleteAreaHidden) {	
									mDeleteAreaLayoutParams.y = (int)((mDisplayHeight)-((2*(mDisplayHeight-mDeleteAreaLayoutParams.y))/3));	
									mWindowManager.updateViewLayout(deleteArea, mDeleteAreaLayoutParams);		
									
									// Cancel animation when the destination is reached
									if (mDisplayHeight<=mDeleteAreaLayoutParams.y){	
										TrayAnimationTimerTask.this.cancel();
										mDeleteAreaAnimationTimer.cancel();
										
									}						
								}
								else
								{
									mDeleteAreaLayoutParams.y = (int)((mDisplayHeight*ANIMATION_DELETEAREA_HEIGHT)+(2*(mDeleteAreaLayoutParams.y-(mDisplayHeight*ANIMATION_DELETEAREA_HEIGHT)))/3);
									mWindowManager.updateViewLayout(deleteArea, mDeleteAreaLayoutParams);	
							
									// Cancel animation when the destination is reached
									if (mDisplayHeight*ANIMATION_DELETEAREA_HEIGHT>=mDeleteAreaLayoutParams.y){	
										TrayAnimationTimerTask.this.cancel();
										mDeleteAreaAnimationTimer.cancel();
									}
								}
								
							break;
								
						default:
							break;
						}
					}
				});
			}
		}
	

	@Override
	  public void onDestroy() {
	    super.onDestroy();	
	    	areViewsDeleted= true;
			 if (mainIconlinearlayout != null) mWindowManager.removeView(mainIconlinearlayout);
			 if (deleteArea != null) mWindowManager.removeView(deleteArea);
			 if	(mAppChooserRootLayout!=null)  mWindowManager.removeView(mAppChooserRootLayout);
	  }


}

