package cz.lala.shortapp.reciever;
import cz.lala.shortapp.setting.Fr_Setting;
import cz.lala.shortapp.startup.StartUpService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class BootCompleteReciever extends BroadcastReceiver {
	
	 final static String TAG = "BootCompletedReceiver";
	private SharedPreferences sharedPreferences;

	@Override
	public void onReceive(Context context, Intent intent) {
		  Log.i(TAG, "starting service...");
		  sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		  if (sharedPreferences.getBoolean(Fr_Setting.BOOT, true)) {
			  context.startService(new Intent(context, StartUpService.class));
		}
	}

}
